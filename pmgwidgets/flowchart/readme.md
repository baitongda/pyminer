# `pmgwidgets.flowchart`

pmgwidgets.flowchart是流程图绘制子包。依赖于networkx和pyqt5。

简单的示例可以直接执行`pmgwidgets/flowchart/flowchart_widget.py`

# `PMFlowWidget`

## 组成部分

![组成部分](doc_figures\组成结构.png)

使用：

```python
from pmgwidgets import PMFlowWidget
app = QApplication(sys.argv)
graphics = PMFlowWidget(path=r'C:\Users\12957\Desktop\乡土中国.pmcache')
graphics.show()
sys.exit(app.exec_())
```

PMFlowWidget可以像普通的QWidget那样嵌套在任何位置。

启动参数：

parent:指定父控件。

path:指定启动时加载的流程图。

PMFlowWidget暂时不支持清空重绘。

## 操作

### 添加节点
![添加节点](doc_figures\添加节点.png)

点击右键菜单的New，即可插入一个新节点。

### 编辑节点

双击节点即可编辑
![节点编辑](doc_figures\节点编辑.png)

- Node Text:节点显示的文字信息
- Set Inputs:输入节点端口编辑。点击`+`可以添加一个端口，点击`-`删除当前端口。双击选项可以编辑文字。
- Set Outputs:输出节点编辑，和输入相同
- Input Python Code:输入Python代码，是节点执行的内容。
### 运行代码
点击`Run`即可运行。节点代码执行后，会在此节点位置显示执行的输入输出结果。

执行之前情况如下![执行之前](doc_figures\执行之前.png)

执行之后情况如下：

![image-20201017130216358](C:\Users\12957\AppData\Roaming\Typora\typora-user-images\image-20201017130216358.png)

### 自定义代码的规范

- 输入参数数量与输入端口数目相同
- 输出参数需要用列表包裹起来，列表长度与输出端口数目相同
- 输入参数从左到右依次为输入端口从上到下的值，输出参数从左到右依次为输出参数从左到右的值。

比如以下代码是规范的：

