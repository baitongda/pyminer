from pyminer_algorithms.linear_algebra.array import array
import numpy
import pytest


def test_array():
    numpy.testing.assert_equal(array([1, 2, 3]), numpy.array([1, 2, 3]))
    numpy.testing.assert_equal(array([1, 2, 3.0]), numpy.array([1.0, 2.0, 3.0]))
    assert array([[1, 2], [3, 4]]).shape == (2, 2)
    assert array([[1, 2], [3, 4]]).dtype == numpy.dtype(int)
    assert array([[1, 2.]]).dtype == numpy.dtype(float)
    assert array([[1, 2]]).dtype == numpy.dtype(int)
    with pytest.raises(AssertionError, match='specify both `ty'):
        array([1, 2, 3], type=int, dtype=int)
