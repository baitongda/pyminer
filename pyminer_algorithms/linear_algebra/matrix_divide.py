import numpy

from .exceptions import LinearAlgebraError


def matrix_divide(a: numpy.ndarray, b: numpy.ndarray) -> numpy.ndarray:
    assert isinstance(a, numpy.ndarray), 'param a should be `numpy.ndarray`'
    assert isinstance(b, numpy.ndarray), 'param b should be `numpy.ndarray`'
    assert len(a.shape) == len(b.shape) == 2, 'only two dimensional array supported'
    assert a.shape[0] == b.shape[0], 'columns(a) should be equal to columns(b)'
    try:
        return numpy.linalg.solve(a, b)
    except numpy.linalg.LinAlgError as exception:
        info = exception.args[0]
        if info == 'Singular matrix':
            raise LinearAlgebraError('matrix a should not be singular matrix')
        raise
