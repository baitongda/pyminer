import numpy


def matrix_dot(a: numpy.ndarray, b: numpy.ndarray) -> numpy.ndarray:
    assert isinstance(a, numpy.ndarray), f'dot only supply numpy.ndarray object, while a is "{type(a)}"'
    assert isinstance(b, numpy.ndarray), f'dot only supply numpy.ndarray object, while a is "{type(b)}"'
    assert a.shape[-1] in (2, 3), f'last dimension of array a should be 2 or 3, not "{a.shape[-1]}"'
    assert b.shape[-1] in (2, 3), f'last dimension of array b should be 2 or 3, not "{b.shape[-1]}"'
    assert a.shape == b.shape, f'two array should have same shape, not "{a.shape}" & "{b.shape}"'
    assert len(a.shape) == 1, f'currently only support 1 dimension, not {len(a.shape)}'
    return numpy.dot(a, b)
