# `shape`函数说明

## 用法

`shape(array)`

## 说明

获取矩阵的结构。

例如，`shape(ones(3,4,5))==(3,4,5)`。

本函数仅用于对`numpy.ndarray`进行操作，
不操作`List[float]`等复合结构的类矩阵的列表。

## 位置参数

1. `array`：任意维度的矩阵。

## 返回值

以元组方式返回矩阵的形状。

## 备注

对于`numpy`用户，本函数简单返回`numpy.ndarray.shape`。

对于`matlab`用户，本函数部分等价于`size`。

# 参考文献

1. [`numpy.ndarray.shape`帮助文档. Numpy.][numpy]
1. [`size`帮助文档. MATLAB.][matlab]

[numpy]: https://numpy.org/doc/stable/reference/generated/numpy.ndarray.shape.html
[matlab]: https://ww2.mathworks.cn/help/matlab/ref/size.html
