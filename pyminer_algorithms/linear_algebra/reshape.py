import re
from typing import Iterable

import numpy

from .exceptions import LinearAlgebraError


def reshape(arr: numpy.ndarray, shape: Iterable[int]):
    assert isinstance(arr, numpy.ndarray), 'first param should be instance of `numpy.ndarray`'
    assert isinstance(shape, tuple), 'second param should be tuple of int'
    for i in shape:
        assert isinstance(i, int), f'shape items should be int, not "{i}"'
        assert i >= 0, f'shape items should be non-negative integers'
    try:
        return numpy.reshape(arr, shape)
    except ValueError as exception:
        if re.match(r'cannot reshape array of size \d+ into shape', exception.args[0]):
            raise LinearAlgebraError(exception.args[0])
        raise
