# `array`函数说明

## 用法

`array(data, **kwargs)`

## 说明

将已有的值转换为一个矩阵。

## 别名

`matrix`

## 位置参数

1. `data`：现有的列表；

## 关键字参数

1. `type=None`: 矩阵的类型，可以是一个`python`类型或者`numpy`类型，
包括`int`、`float`等。`type`也可以写为`dtype`，以满足`numpy`用户的习惯。
如果不指定`type`，则`PyMiner`会自己根据数据判断类型。
具体支持的类型请参照[numpy的类型][numpy的类型]。

## 返回值

根据指定的数据、指定的类型生成矩阵。

## 备注

本函数与`numpy.array`有较大的差别，
如果需要进行底层的`array`控制，请采用类似如下方式以访问`numpy`原生的`array`：

```python
import numpy as np
np.array([1,2,3], order='K')
```

[numpy的类型]: https://numpy.org/devdocs/user/basics.types.html

# 参考文献

1. [`array`帮助文档. Numpy.][numpy]

[numpy]: https://numpy.org/doc/stable/reference/generated/numpy.array.html
