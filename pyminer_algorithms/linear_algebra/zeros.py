import numpy

from . import utils


def zeros(shape, *shapes, type=float, dtype=None) -> numpy.ndarray:
    shape = utils.preprocess_shape(shape, *shapes)
    type = utils.preprocess_type(type, dtype)
    return numpy.zeros(shape, dtype=type)
