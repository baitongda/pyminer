import numpy


def matrix_determinant(arr: numpy.ndarray):
    assert isinstance(arr, numpy.ndarray), f'only `numpy.ndarray` supported, not "{type(arr)}"'
    assert len(arr.shape) == 2, f'determinant is only valid for 2d array, not "{arr.shape}"'
    assert arr.shape[0] == arr.shape[1], f'determinant is only valid for square matrix, not "{arr.shape}"'
    return numpy.linalg.det(arr)
