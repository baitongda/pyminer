# `matrix_diagonal`函数说明

## 用法

`matrix_diagonal(arr, k=0)`

## 说明

如果参数是一维矩阵，则以其为对角线生成二维矩阵。

如果参数是二维矩阵，则返回其对角线上的元素。

通过指定参数`k`可以选择将对角线进行上下平移。

## 位置参数

1. `arr`：用于生成矩阵的对角线元素，或者用于获取对角线的矩阵。
1. `k`: 对角线的位置，`0`为主对角线，`1`为主对角线向上一层，`-1`为主对角线向下一层。

## 返回值

如果参数是二维矩阵，则返回其对角线上的元素。

如果参数是一维矩阵，则返回根据其生成的二维矩阵。

# 参考文献

1. [`diag`帮助文档. Numpy.][np]
1. [`matlab`帮助文档. MATLAB.][ml]

[np]: https://numpy.org/doc/stable/reference/generated/numpy.diag.html
[ml]: https://ww2.mathworks.cn/help/matlab/ref/diag.html